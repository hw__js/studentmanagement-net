﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class ManageScoresForm : Form
    {
        public ManageScoresForm()
        {
            InitializeComponent();
        }

        SCORE score = new SCORE();
        STUDENT student = new STUDENT();
        COURSE course = new COURSE();
        string data = "score";
        private void ManageScoresForm_Load(object sender, EventArgs e)
        {
            //populate the combo box with course
            comboBoxCourse.DataSource = course.getAllCourse();
            comboBoxCourse.DisplayMember = "label";
            comboBoxCourse.ValueMember = "Id";

            //populate the datagridview with student score
            dataGridView1.DataSource = score.getStudentsScore();
        }

        // display students data on datagridview
        private void buttonShowStudents_Click(object sender, EventArgs e)
        {
            data = "student";
            SqlCommand command = new SqlCommand("SELECT id, fname, lname, bdate FROM std");
            dataGridView1.DataSource = student.getStudents(command);
        }

        // display scores data on datagridview
        private void buttonShowScores_Click(object sender, EventArgs e)
        {
            data = "score";
            dataGridView1.DataSource = score.getStudentsScore();
        }

        // get data from datagridview
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            getDataFromDatagridview();
        }

        // create a function to get data from datagridview
        public void getDataFromDatagridview()
        {
            // if the user select to show student data then we will show only student id
            if (data == "student")
            {
                textBoxStudentID.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();

            }
            // if the user select to show score data then we will show student id + select the course from the combobox
            else if (data == "score")
            {
                textBoxStudentID.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                comboBoxCourse.SelectedValue = dataGridView1.CurrentRow.Cells[3].Value;
            }

        }

        // button add score
        private void btnAddScore_Click(object sender, EventArgs e)
        {
            try
            {
                //ad new score
                int studentId = Convert.ToInt32(textBoxStudentID.Text);
                int courseId = Convert.ToInt32(comboBoxCourse.SelectedValue);
                float scoreValue = (float)Convert.ToDouble(textBoxScore.Text);
                string description = txtDescription.Text;

                //check if a score is already asigned to this student in this course
                if (!score.studentScoreExists(studentId, courseId))
                {
                    if (score.insertScore(studentId, courseId, scoreValue, description))
                    {
                        MessageBox.Show("Student Score Inserted", "Add Score", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Student Score Not Inserted", "Add Score", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("The Score For This Course Are Already Set", "Add Score", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Add Score", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // button remove score
        private void btnRemoveScore_Click(object sender, EventArgs e)
        {
            // remove the selected course
            int studentId = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            int courseId = int.Parse(dataGridView1.CurrentRow.Cells[3].Value.ToString());

            if (MessageBox.Show("Do You Want to Delete This Score?", "Delete Score", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (score.deteleScore(studentId, courseId))
                {
                    MessageBox.Show("Score Deleted", "Remove Score", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.DataSource = score.getStudentsScore();
                }
                else
                {
                    MessageBox.Show("Score Not Deleted", "Remove Score", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        // show a new form with the average score by course
        private void btnAvgScore_Click(object sender, EventArgs e)
        {
            avgScoreByCourseForm avgScrByCrsf = new avgScoreByCourseForm();
            avgScrByCrsf.Show(this);
        }
    }
}
