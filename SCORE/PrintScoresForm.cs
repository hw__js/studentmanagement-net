﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SautinSoft.Document;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace Lab08
{
    public partial class PrintScoresForm : Form
    {

        public PrintScoresForm()
        {
            InitializeComponent();
        }

        SCORE score = new SCORE();
        COURSE course = new COURSE();
        STUDENT student = new STUDENT();
        private void PrintScoresForm_Load(object sender, EventArgs e)
        {
            // populate datagridview with students data
            dataGridView1.DataSource = student.getStudents(new SqlCommand("SELECT Id, fname, lname FROM std"));
            // 2 ways change header name:
            // dataGridView1.Columns["Old Column Name"].HeaderText = "New Grid Column Name";
            // dataGridView1.Columns[column_index].HeaderText = "New Grid Column Name";
            dataGridView1.Columns[1].HeaderText = "first_name";
            dataGridView1.Columns[2].HeaderText = "last_name";

            // populate datagridview with scores data
            dataGridViewStudentsScore.DataSource = score.getStudentsScore();
            dataGridViewStudentsScore.Columns[1].HeaderText = "first_name";
            dataGridViewStudentsScore.Columns[2].HeaderText = "last_name";

            // populate datagridview with courses data
            listBoxCourses.DataSource = course.getAllCourse();
            listBoxCourses.DisplayMember = "label";
            listBoxCourses.ValueMember = "Id";

        }

        // When you select a course from the listbox
        // all scores assigned to this course will be displayed in the datagridview
        private void listBoxCourses_Click(object sender, EventArgs e)
        {
            dataGridViewStudentsScore.DataSource = score.getCourseScores(int.Parse(listBoxCourses.SelectedValue.ToString()));
        }

        // display the selected student scores
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            dataGridViewStudentsScore.DataSource = score.getStudentScores(int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString()));
        }

        // populate datagridview with all scores data
        private void labelReset_Click(object sender, EventArgs e)
        {
            dataGridViewStudentsScore.DataSource = score.getStudentsScore();
            dataGridViewStudentsScore.Columns[1].HeaderText = "first_name";
            dataGridViewStudentsScore.Columns[2].HeaderText = "last_name";
        }

        // print score data from datagridview to text file
        private void ButtonSave_Click(object sender, EventArgs e)
        {
            // our file path
            // the file name = scores_list.txt
            // location = in the desktop
            //D:\spkt\nam3(ki2)\WindowPrograming\week1\Lab08\textprint\scores_list.txt
            //string path = @"D:\spkt\nam3(ki2)\WindowPrograming\week1\Lab08\textprint\scores_list.docx";
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\scores_list.txt";

            using (var writer = new StreamWriter(path))
            {
                // check if the file exists
                if (!File.Exists(path))
                {
                    File.Create(path);
                }


                // rows
                for (int i = 0; i < dataGridViewStudentsScore.Rows.Count; i++)
                {
                    // columns
                    for (int j = 0; j < dataGridViewStudentsScore.Columns.Count; j++)
                    {
                        writer.Write("\t" + dataGridViewStudentsScore.Rows[i].Cells[j].Value.ToString() + "\t" + "|");
                    }
                    // Make a new line 
                    writer.WriteLine("");
                    // Make a seperation
                    //writer.WriteLine("-------------------------------------------------------------");
                }

                writer.Close();
                MessageBox.Show("Data Exported");
            }
        }

        private void button_ExportToPDF_Click(object sender, EventArgs e)
        {
            if (dataGridViewStudentsScore.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PDF (*.pdf)|*.pdf";
                sfd.FileName = "Output.pdf"; bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            PdfPTable pdfTable = new PdfPTable(dataGridViewStudentsScore.Columns.Count);
                            pdfTable.DefaultCell.Padding = 3;
                            pdfTable.WidthPercentage = 100;
                            pdfTable.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                            foreach (DataGridViewColumn column in dataGridViewStudentsScore.Columns)
                            {
                                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                                pdfTable.AddCell(cell);
                            }
                            foreach (DataGridViewRow row in dataGridViewStudentsScore.Rows)
                            {
                                string studentId = row.Cells[0].Value.ToString();
                                pdfTable.AddCell(studentId);
                                string Fname = row.Cells[1].Value.ToString();
                                pdfTable.AddCell(Fname);
                                string lname = row.Cells[2].Value.ToString();
                                pdfTable.AddCell(lname);
                                string courseId = row.Cells[3].Value.ToString();
                                pdfTable.AddCell(courseId);
                                string Name_Course = row.Cells[4].Value.ToString();
                                pdfTable.AddCell(Name_Course);
                                string student_score = row.Cells[5].Value.ToString();
                                pdfTable.AddCell(student_score);
                            }
                            using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                            {
                                Document pdfDoc = new Document(PageSize.A4, 10f, 20f, 20f, 10f);
                                PdfWriter.GetInstance(pdfDoc, stream); pdfDoc.Open();
                                pdfDoc.Add(pdfTable);
                                pdfDoc.Close();
                                stream.Close();
                            }

                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }
    }
}
