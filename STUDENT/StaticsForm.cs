﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class StaticsForm : Form
    {
        public StaticsForm()
        {
            InitializeComponent();
        }

        // dat mau neu muon
        Color panTotalColor;
        Color panMaleColor;
        Color panFemaleColor;
        
        private void StaticsForm_Load(object sender, EventArgs e)
        {
            // get panels color
            panTotalColor = PanelTotal.BackColor;
            panMaleColor = PanelMale.BackColor;
            panFemaleColor = PanelFemale.BackColor;

            // display the values
            STUDENT student = new STUDENT();
            double total = Convert.ToDouble(student.totalStudent());
            double totalMale = Convert.ToDouble(student.totalMaleStudent());
            double totalFemale = Convert.ToDouble(student.totalFemaleStudent());
            // tinh %, cac ban xem lai phep toan
            // (tong student x 100) / (total students)
            double maleStudentPercentage = (totalMale * (100 / total));
            double femaleStudentPercentage = (totalFemale * (100 / total));
            LabelTotal.Text = ("Total Students: " + total.ToString());
            LabelMale.Text = ("Male:" + (maleStudentPercentage.ToString("0.00") + "%"));
            LabelFemale.Text = ("Female:" + (femaleStudentPercentage.ToString("0.00") + "%"));
        }

        private void LabelTotal_MouseEnter(object sender, EventArgs e)
        {
            PanelTotal.BackColor = Color.White;
            LabelTotal.ForeColor = panTotalColor;
        }

        private void LabelTotal_MouseLeave(object sender, EventArgs e)
        {
            PanelTotal.BackColor = panTotalColor;
            LabelTotal.ForeColor = Color.White;
        }

        private void LabelMale_MouseEnter(object sender, EventArgs e)
        {
            PanelMale.BackColor = Color.White;
            LabelMale.ForeColor = panMaleColor;
        }

        private void LabelMale_MouseLeave(object sender, EventArgs e)
        {
            PanelMale.BackColor = panMaleColor;
            LabelMale.ForeColor = Color.White;
        }

        private void LabelFemale_MouseEnter(object sender, EventArgs e)
        {
            PanelFemale.BackColor = Color.White;
            LabelFemale.ForeColor = panFemaleColor;
        }

        private void LabelFemale_MouseLeave(object sender, EventArgs e)
        {
            PanelFemale.BackColor = panFemaleColor;
            LabelFemale.ForeColor = Color.White;
        }
        // thu cac event lam viec voi mouse
    }
}
