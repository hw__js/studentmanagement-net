﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class ResultForm : Form
    {
        public ResultForm()
        {
            InitializeComponent();
        }

        STUDENT student = new STUDENT();
        SCORE score = new SCORE();
        private void ResultForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = score.LoadResultForm();
            dataGridView1.Columns["Id1"].Visible = false;
            dataGridView1.Columns["AverageScore"].DefaultCellStyle.Format = "N2";
            //dataGridView1.Columns[0].HeaderText = "Student id";
            //dataGridView1.Columns[1].HeaderText = "First Name";
            //dataGridView1.Columns[3].HeaderText = "Last Name";
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            textBoxStudentID.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBoxFirstName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBoxLastName.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            DataView dv = score.LoadResultForm().DefaultView;
            dv.RowFilter = string.Format("fname like '%{0}%' OR Convert([id], System.String) like '%{0}%'", textBoxSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = score.LoadResultForm();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            // our file path
            // the file name = scores_list.txt
            // location = in the desktop
            //D:\spkt\nam3(ki2)\WindowPrograming\week1\Lab08\textprint\scores_list.txt
            //string path = @"D:\spkt\nam3(ki2)\WindowPrograming\week1\Lab08\textprint\scores_list.docx";
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\resultform_list.txt";

            using (var writer = new StreamWriter(path))
            {
                // check if the file exists
                if (!File.Exists(path))
                {
                    File.Create(path);
                }


                // rows
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    // columns
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        writer.Write("\t" + dataGridView1.Rows[i].Cells[j].Value.ToString() + "\t" + "|");
                    }
                    // Make a new line 
                    writer.WriteLine("");
                    // Make a seperation
                    //writer.WriteLine("-------------------------------------------------------------");
                }

                writer.Close();
                MessageBox.Show("Data Exported");
            }
        }
    }
}
