﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class Chart : Form
    {
        public Chart()
        {
            InitializeComponent();
        }

        private void Chart_Load(object sender, EventArgs e)
        {
            // display the values
            SCORE score = new SCORE();
            STUDENT student = new STUDENT();
            double statisCSharp = Convert.ToDouble(score.getStaticCourse(10));
            double statisJava = Convert.ToDouble(score.getStaticCourse(8));
            double statisCloud = Convert.ToDouble(score.getStaticCourse(1));
            double statisMachine = Convert.ToDouble(score.getStaticCourse(9));

            // show chart
            chart1.Series["Score"].Points.AddXY("C#", statisCSharp);
            chart1.Series["Score"].Points.AddXY("Java", statisJava);
            chart1.Series["Score"].Points.AddXY("Icloud Computing", statisCloud);
            chart1.Series["Score"].Points.AddXY("Machine Learning", statisMachine);

            double total = Convert.ToDouble(student.totalStudent());
            double totalPass = Convert.ToDouble(score.getStaticPass());
            double totalFail = Convert.ToDouble(score.getStaticFail());

            double StudentPassPercentage = (totalPass * (100 / total));
            double StudentFailPercentage = (totalFail * (100 / total));

            // show Pie Chart
            Piechart.Series["Rate"].Points.AddXY("Pass", StudentPassPercentage);
            Piechart.Series["Rate"].Points.AddXY("Fail", StudentFailPercentage);
        }
    }
}
