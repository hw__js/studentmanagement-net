﻿namespace Lab08
{
    partial class StaticResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StaticResult));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPass = new System.Windows.Forms.Label();
            this.labelJava = new System.Windows.Forms.Label();
            this.labelCloud = new System.Windows.Forms.Label();
            this.labelMachine = new System.Windows.Forms.Label();
            this.labelFail = new System.Windows.Forms.Label();
            this.labelCSharp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.IndianRed;
            this.label1.Location = new System.Drawing.Point(53, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Static By Course";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.IndianRed;
            this.label2.Location = new System.Drawing.Point(421, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 34);
            this.label2.TabIndex = 0;
            this.label2.Text = "Static By Result";
            // 
            // labelPass
            // 
            this.labelPass.AutoSize = true;
            this.labelPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelPass.Location = new System.Drawing.Point(424, 121);
            this.labelPass.Name = "labelPass";
            this.labelPass.Size = new System.Drawing.Size(67, 25);
            this.labelPass.TabIndex = 1;
            this.labelPass.Text = "Pass:";
            // 
            // labelJava
            // 
            this.labelJava.AutoSize = true;
            this.labelJava.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelJava.Location = new System.Drawing.Point(56, 176);
            this.labelJava.Name = "labelJava";
            this.labelJava.Size = new System.Drawing.Size(66, 25);
            this.labelJava.TabIndex = 1;
            this.labelJava.Text = "Java:";
            // 
            // labelCloud
            // 
            this.labelCloud.AutoSize = true;
            this.labelCloud.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelCloud.Location = new System.Drawing.Point(54, 235);
            this.labelCloud.Name = "labelCloud";
            this.labelCloud.Size = new System.Drawing.Size(186, 25);
            this.labelCloud.TabIndex = 1;
            this.labelCloud.Text = "Cloud Computing:";
            // 
            // labelMachine
            // 
            this.labelMachine.AutoSize = true;
            this.labelMachine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelMachine.Location = new System.Drawing.Point(54, 294);
            this.labelMachine.Name = "labelMachine";
            this.labelMachine.Size = new System.Drawing.Size(191, 25);
            this.labelMachine.TabIndex = 1;
            this.labelMachine.Text = "Machine Learning:";
            // 
            // labelFail
            // 
            this.labelFail.AutoSize = true;
            this.labelFail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelFail.Location = new System.Drawing.Point(424, 176);
            this.labelFail.Name = "labelFail";
            this.labelFail.Size = new System.Drawing.Size(54, 25);
            this.labelFail.TabIndex = 1;
            this.labelFail.Text = "Fail:";
            // 
            // labelCSharp
            // 
            this.labelCSharp.AutoSize = true;
            this.labelCSharp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labelCSharp.Location = new System.Drawing.Point(56, 121);
            this.labelCSharp.Name = "labelCSharp";
            this.labelCSharp.Size = new System.Drawing.Size(47, 25);
            this.labelCSharp.TabIndex = 1;
            this.labelCSharp.Text = "C#:";
            // 
            // StaticResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(668, 368);
            this.Controls.Add(this.labelMachine);
            this.Controls.Add(this.labelCloud);
            this.Controls.Add(this.labelJava);
            this.Controls.Add(this.labelFail);
            this.Controls.Add(this.labelCSharp);
            this.Controls.Add(this.labelPass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StaticResult";
            this.Text = "StaticResult";
            this.Load += new System.EventHandler(this.StaticResult_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPass;
        private System.Windows.Forms.Label labelJava;
        private System.Windows.Forms.Label labelCloud;
        private System.Windows.Forms.Label labelMachine;
        private System.Windows.Forms.Label labelFail;
        private System.Windows.Forms.Label labelCSharp;
    }
}