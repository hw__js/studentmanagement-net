﻿using System;
using System.Windows.Forms;

namespace Lab08
{
    public partial class StaticResult : Form
    {
        public StaticResult()
        {
            InitializeComponent();
        }

        private void StaticResult_Load(object sender, EventArgs e)
        {
            // display the values
            SCORE score = new SCORE();
            STUDENT student = new STUDENT();
            double statisCSharp = Convert.ToDouble(score.getStaticCourse(10));
            double statisJava = Convert.ToDouble(score.getStaticCourse(8));
            double statisCloud = Convert.ToDouble(score.getStaticCourse(1));
            double statisMachine = Convert.ToDouble(score.getStaticCourse(9));
            
            labelCSharp.Text = ("C#: " + statisCSharp.ToString("0.00"));
            labelJava.Text = ("Java#: " + statisJava.ToString());
            labelCloud.Text = ("Cloud Computing: " + statisCloud.ToString());
            labelMachine.Text = ("Machine Learning: " + statisMachine.ToString("0.00"));

            double total = Convert.ToDouble(student.totalStudent());
            double totalPass = Convert.ToDouble(score.getStaticPass());
            double totalFail = Convert.ToDouble(score.getStaticFail());
            // tinh %, cac ban xem lai phep toan
            // (tong student x 100) / (total students)
            double StudentPassPercentage = (totalPass * (100 / total));
            double StudentFailPercentage = (totalFail * (100 / total));
            labelPass.Text = ("Pass:" + (StudentPassPercentage.ToString("0.00") + "%"));
            labelFail.Text = ("Fail:" + (StudentFailPercentage.ToString("0.00") + "%"));
        }
    }
}
