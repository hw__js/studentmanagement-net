﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace Lab08
{
    public partial class MainForm01 : Form
    {
        public MainForm01()
        {
            InitializeComponent();
        }

        private void addNewStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddStudentForm addStdF = new AddStudentForm();
            addStdF.Show(this);
        }
        private void studentsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
                studentsListForm sLF = new studentsListForm();
                var topLeftHeaderCell = sLF.dataGridView1.TopLeftHeaderCell; // Make sure TopLeftHeaderCell is created
                sLF.Show(this);
        }

        private void editRemoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateDeleteStudentForm udsf = new UpdateDeleteStudentForm();
            udsf.Show();
        }

        private void staticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticsForm stcF = new StaticsForm();
            stcF.Show();
        }

        private void manageStudentFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageStudentsForm mngStdF = new ManageStudentsForm();
            mngStdF.Show();
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintStudentsForm prStdF = new PrintStudentsForm();
            var topLeftHeaderCell = prStdF.dataGridView1.TopLeftHeaderCell; // Make sure TopLeftHeaderCell is created
            prStdF.Show();
        }

        private void addCourseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddCourse addCrsF = new AddCourse();
            addCrsF.Show();
        }

        private void removeCourseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveCourseForm rmvCrsF = new RemoveCourseForm();
            rmvCrsF.Show();
        }

        private void editCourseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditCourseForm editCrsF = new EditCourseForm();
            editCrsF.Show();
        }

        private void manageCoursesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageCoursesForm mngCrsF = new ManageCoursesForm();
            mngCrsF.Show();
        }

        private void printToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PrintCoursesForm prCrsF = new PrintCoursesForm();
            prCrsF.Show();
        }

        private void MainForm01_Load(object sender, EventArgs e)
        {
        }

        private void addScoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddScoreForm addScrF = new AddScoreForm();
            addScrF.Show();
        }

        private void removeScoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveScoreForm rmvScrF = new RemoveScoreForm();
            rmvScrF.Show();
        }

        private void manageScoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageScoresForm mngScrF = new ManageScoresForm();
            mngScrF.Show();
        }

        private void avgScoreByCourseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            avgScoreByCourseForm avgScrByCrsf = new avgScoreByCourseForm();
            avgScrByCrsf.Show();
        }

        private void printToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            PrintScoresForm pScrf = new PrintScoresForm();
            pScrf.Show();
        }

        private void aVGResultByScoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResultForm rF = new ResultForm();
            rF.Show();
        }

        private void staticResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticResult SF = new StaticResult();
            SF.Show();
        }

        WindowsMediaPlayer player = new WindowsMediaPlayer();
        private void playMusicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            player.URL = "G:\\TaLaCuaNhau-DongNhiOngCaoThang-4113753.mp3";
            player.controls.play();
        }

        private void stopMusicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            player.controls.stop();
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("STUDENT MANAGEMENT STUDENT \nWinform Programming Course \n-Teacher: Le Vinh Thinh\n-SV thực hiện:\n  1. Le Trinh Hoang Phu 18110037", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }

        private void resetPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetPassword rstPass = new ResetPassword();
            rstPass.Show(this);
        }

        private void chartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Chart chart = new Chart();
            chart.Show(this);
        }
    }
}
