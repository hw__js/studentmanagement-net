﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab08
{
    class COURSE
    {
        MY_DB mydb = new MY_DB();
        public bool insertCourse(int ID, string courseName, int hoursNumber, string description)
        {
            SqlCommand command = new SqlCommand("INSERT INTO Course (Id, label, period, description)" + "VALUES (@id,@name, @hrs, @dscr)", mydb.getConnection);
            command.Parameters.Add("@id", SqlDbType.Int).Value = ID;
            command.Parameters.Add("@name", SqlDbType.VarChar).Value = courseName;
            command.Parameters.Add("@hrs", SqlDbType.Int).Value = hoursNumber;
            command.Parameters.Add("@dscr", SqlDbType.Text).Value = description;

            mydb.openConnection();

            if (command.ExecuteNonQuery() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // create a function to check if the course name already exists in the database
        // when we edit a course we need to exclude the current course from the name verification
        // using the course id
        // by defaul we will set the course id to 0 
        public bool checkCourseName(string courseName, int courseID = 0)
        {

            SqlCommand command = new SqlCommand("SELECT * FROM Course WHERE label = @cName AND id <> @cid", mydb.getConnection);

            command.Parameters.Add("@cid", SqlDbType.Int).Value = courseID;
            command.Parameters.Add("@cName", SqlDbType.VarChar).Value = courseName;


            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();

            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // function to remove course by id
        public bool deleteCourse(int courseId)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Course WHERE id=@ID", mydb.getConnection);
            command.Parameters.Add("@ID", SqlDbType.Int).Value = courseId;
            mydb.openConnection();
            if (command.ExecuteNonQuery() == 1)
            {
                return true;

            }
            else
            {
                return false;
            }

        }

        // create a function to get all courses
        public DataTable getAllCourse()
        {
            SqlCommand command = new SqlCommand("SELECT *  FROM Course", mydb.getConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();

            adapter.Fill(table);

            return table;
        }

        // create a function to get a course by id
        public DataTable getCourseById(int courseID)
        {
            SqlCommand command = new SqlCommand("SELECT *  FROM Course WHERE id = @cid", mydb.getConnection);

            command.Parameters.Add("@cid", SqlDbType.Int).Value = courseID;

            SqlDataAdapter adapter = new SqlDataAdapter(command);

            DataTable table = new DataTable();

            adapter.Fill(table);

            return table;
        }

        // create a function to edit the selected course
        public bool updateCourse(int courseId, string courseName, int hoursNumber, string description)
        {
            SqlCommand command = new SqlCommand("UPDATE Course SET label=@name, period = @hrs, description = @dscr WHERE Id=@cid", mydb.getConnection);

            command.Parameters.Add("@cid", SqlDbType.Int).Value = courseId;
            command.Parameters.Add("@name", SqlDbType.VarChar).Value = courseName;
            command.Parameters.Add("@hrs", SqlDbType.Int).Value = hoursNumber;
            command.Parameters.Add("@dscr", SqlDbType.Text).Value = description;

            mydb.openConnection();

            if (command.ExecuteNonQuery() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Create the function to execute the count queries
        public string execCount(string query)
        {
            SqlCommand command = new SqlCommand(query, mydb.getConnection);
            mydb.openConnection();
            string count = command.ExecuteScalar().ToString();
            mydb.closeConnection();
            return count;
        }

        // Get the total courses
        public string totalCourses()
        {
            return execCount("Select COUNT(*) From Course ");
        }
    }
}


