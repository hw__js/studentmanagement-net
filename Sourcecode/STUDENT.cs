﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab08
{
    class STUDENT
    {
        MY_DB mydb = new MY_DB();
        // Function to insert a new student
        public bool insertStudent(int Id, string fname, string lname, DateTime bdate, string gender, string phone, string address, MemoryStream picture)
        {
            SqlCommand command = new SqlCommand("INSERT INTO std (id, fname, lname, bdate, gender, phone, address, picture)"+
                "VALUES (@id, @fn, @ln, @bdt, @gdr, @phn, @adrs, @pic)",mydb.getConnection);
            command.Parameters.Add("@id", SqlDbType.Int).Value = Id;
            command.Parameters.Add("@fn", SqlDbType.VarChar).Value = fname;
            command.Parameters.Add("@ln", SqlDbType.VarChar).Value = lname;
            command.Parameters.Add("@bdt", SqlDbType.DateTime).Value = bdate;
            command.Parameters.Add("@gdr", SqlDbType.VarChar).Value = gender;
            command.Parameters.Add("@phn", SqlDbType.VarChar).Value = phone;
            command.Parameters.Add("@adrs", SqlDbType.VarChar).Value = address;
            command.Parameters.Add("@pic", SqlDbType.Image).Value = picture.ToArray();

            mydb.openConnection();
           
            if((command.ExecuteNonQuery() == 1))
            {
                mydb.closeConnection();
                return true;
            }    
            else
            {
                mydb.closeConnection();
                return false;
            }    
        }
        // function to get all students from database
        public DataTable getStudents(SqlCommand command)
        {
            command.Connection = mydb.getConnection;
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable table = new DataTable();
            adapter.SelectCommand = command;
            adapter.Fill(table);
            return table;
        }

        // delete student by id (Use in UpdateDeleteStudentForm.cs)
        public bool deleteStudent(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM std WHERE id = @id", mydb.getConnection);
            command.Parameters.Add("@id", SqlDbType.Int).Value = id;
            mydb.openConnection();
            if ((command.ExecuteNonQuery() == 1)) 
            {
                mydb.closeConnection();
                return true;
            }    
            else
            {
                mydb.closeConnection();
                return false;
            }    
        }

        // update a student information
        public bool updateStudent(int id, string fname, string lname, DateTime bdate, string gender, string phone, string address, MemoryStream picture)
        {
            SqlCommand command = new SqlCommand("UPDATE std SET fname=@fn,lname=@ln,bdate=@bdt,gender=@gdr,phone=@phn,ad" + "dress=@adrs,picture=@pic WHERE id=@ID", mydb.getConnection);

            command.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            command.Parameters.Add("@fn", SqlDbType.VarChar).Value = fname;
            command.Parameters.Add("@ln", SqlDbType.VarChar).Value = lname;
            command.Parameters.Add("@bdt", SqlDbType.DateTime).Value = bdate;
            command.Parameters.Add("@gdr", SqlDbType.VarChar).Value = gender;
            command.Parameters.Add("@phn", SqlDbType.VarChar).Value = phone;
            command.Parameters.Add("@adrs", SqlDbType.VarChar).Value = address;
            command.Parameters.Add("@pic", SqlDbType.Image).Value = picture.ToArray();

            mydb.openConnection();

            if ((command.ExecuteNonQuery() == 1)) 
            {
                mydb.closeConnection();
                return true;
            }
            else 
            {
                mydb.closeConnection();
                return false;
            }
        }

        // Create the function to execute the count queries
        public string execCount(string query)
        {
            SqlCommand command = new SqlCommand(query, mydb.getConnection);
            mydb.openConnection();
            string count = command.ExecuteScalar().ToString();
            mydb.closeConnection();
            return count;
        }

        // Get the total students
        public string totalStudent()
        {
            return execCount("Select COUNT(*) From std ");
        }
        // Get the total male students
        public string totalMaleStudent()
        {
            return execCount("Select COUNT(*) From std WHERE gender = 'Male'");
        }
        // Get the total female students
        public string totalFemaleStudent()
        {
            return execCount("Select COUNT(*) From std WHERE gender = 'Female'");
        }
    }
}
