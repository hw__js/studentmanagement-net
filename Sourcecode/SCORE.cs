﻿using System.Data;
using System.Data.SqlClient;

namespace Lab08
{
    class SCORE
    {
        MY_DB mydb = new MY_DB();

        // create a function to insert a new score
        public bool insertScore(int studentId, int courseId, float score, string description)
        {
            SqlCommand command = new SqlCommand("INSERT INTO score (student_id, course_id, student_score, description) VALUES (@sid, @cid, @scr ,@dscr)", mydb.getConnection);
            command.Parameters.Add("@sid", SqlDbType.Int).Value = studentId;
            command.Parameters.Add("@cid", SqlDbType.Int).Value = courseId;
            command.Parameters.Add("@scr", SqlDbType.Float).Value = score;
            command.Parameters.Add("dscr", SqlDbType.VarChar).Value = description;

            mydb.openConnection();

            if ((command.ExecuteNonQuery() == 1))
            {
                mydb.closeConnection();
                return true;
            }
            else
            {
                mydb.closeConnection();
                return false;
            }
        }

        // Create a function to check if a score is already assigned to this student in this course
        public bool studentScoreExists(int studentId, int courseId)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM score WHERE student_id = @sid AND course_id= @cid", mydb.getConnection);
            command.Parameters.Add("@sid", SqlDbType.Int).Value = studentId;
            command.Parameters.Add("@cid", SqlDbType.Int).Value = courseId;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();

            adapter.Fill(table);

            if ((table.Rows.Count == 0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        // Create a function to get students score
        public DataTable getStudentsScore()
        {
            SqlCommand command = new SqlCommand();
            command.Connection = mydb.getConnection;
            command.CommandText = ("SELECT score.student_id, std.fname, std.lname, score.course_id, Course.label, score.student_score FROM std INNER JOIN score ON std.id = score.student_id INNER JOIN Course ON score.course_id = Course.Id");

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            return table;
        }

        // function to remove score by student id and course id
        public bool deteleScore(int studentId, int courseId)
        {
            SqlCommand command = new SqlCommand("DELETE FROM score WHERE student_id=@sid AND course_id=@cid", mydb.getConnection);
            
            command.Parameters.Add("@sid", SqlDbType.Int).Value = studentId;
            command.Parameters.Add("@cid", SqlDbType.Int).Value = courseId;
           

            mydb.openConnection();

            if ((command.ExecuteNonQuery() == 1))
            {
                mydb.closeConnection();
                return true;
            }
            else
            {
                mydb.closeConnection();
                return false;
            }
        }

        // create a function to get the average score by course
        public DataTable getAvgScoreByCourse()
        {
            SqlCommand command = new SqlCommand();
            command.Connection = mydb.getConnection;
            command.CommandText = ("SELECT Course.label, AVG(score.student_score) AS AverageGrade FROM Course, score WHERE Course.Id = score.course_id GROUP BY  Course.label ");

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            return table;
        }

        // create courses scores
        public DataTable getCourseScores(int courseId)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = mydb.getConnection;
            command.CommandText = ("SELECT score.student_id, std.fname, std.lname, score.course_id, Course.label, score.student_score FROM std INNER JOIN score ON std.id = score.student_id INNER JOIN Course ON score.course_id = Course.Id WHERE score.course_id =" + courseId);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            return table;
        }

        // create students scores
        public DataTable getStudentScores(int studentId)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = mydb.getConnection;
            command.CommandText = ("SELECT score.student_id, std.fname, std.lname, score.course_id, Course.label, score.student_score FROM std INNER JOIN score ON std.id = score.student_id INNER JOIN Course ON score.course_id = Course.Id WHERE score.student_id =" + studentId);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            return table;
        }

        //========== Result Form================
        public DataTable LoadResultForm()
        {
            SqlCommand command = new SqlCommand();
            command.Connection = mydb.getConnection;
            command.CommandText = ("IF OBJECT_ID('tempdb..##Temp') IS NOT NULL DROP TABLE ##Temp DECLARE @ListToPivot AS NVARCHAR(MAX) SELECT @ListToPivot = COALESCE(@ListToPivot + ',', '') + QUOTENAME(label) From Course DECLARE @SqlStatement NVARCHAR(MAX) SET @SqlStatement = N' SELECT* FROM( SELECT std.Id, std.fname, std.lname, Course.label , score.student_score FROM score, std, Course WHERE score.student_id = std.Id AND score.course_id = Course.Id ) StudentResults PIVOT( SUM(student_score) FOR[label] IN( '+@ListToPivot+' ) ) AS PivotTable' set @SqlStatement = 'select * into ##Temp from (' + @SqlStatement + ') y' EXEC(@SqlStatement) SELECT* FROM(SELECT* FROM ##Temp) AS t1 INNER JOIN (Select distinct std.Id, (sum(student_score)/ (Select Count(DISTINCT label) From Course )) AS AverageScore, case when(sum(student_score) / (Select Count(DISTINCT label) From Course)) >= 5.0 then 'pass'  when(sum(student_score) / (Select Count(DISTINCT label) From Course)) < 5.0 then 'fail' else NULL END AS Result from score, std, Course where score.student_id = std.Id AND Course.Id = score.course_id group by std.Id) AS t2 ON (t1.Id = t2.Id)");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            return table;
        }
        // Create the function to execute the count queries
        public string execCount(string query)
        {
            SqlCommand command = new SqlCommand(query, mydb.getConnection);
            mydb.openConnection();
            string count = command.ExecuteScalar().ToString();
            mydb.closeConnection();
            return count;
        }
        // Get the total students
        public string getStaticCourse(int courseid)
        {
            SqlCommand command = new SqlCommand("Select dbo.GetGrade(@id) As AverageGrade", mydb.getConnection);
            command.Parameters.Add("@id", SqlDbType.Int).Value = courseid;
            mydb.openConnection();
            string count = command.ExecuteScalar().ToString();
            mydb.closeConnection();
            return count;
        }
        public string getStaticPass()
        {
            return execCount("Insert Into dbo.average Select distinct std.Id, (sum(student_score) / (Select Count(DISTINCT label) From Course )) AS AverageScore, case when(sum(student_score) / (Select Count(DISTINCT label) From Course)) >= 5.0 then 'pass'  when(sum(student_score) / (Select Count(DISTINCT label) From Course)) < 5.0 then 'fail' else NULL END AS Result from score, std, Course where score.student_id = std.Id AND Course.Id = score.course_id group by std.Id  Select count(Result) from average Where Result = 'pass' DELETE FROM dbo.average; ");
        }
        public string getStaticFail()
        {
            return execCount("Insert Into dbo.average Select distinct std.Id, (sum(student_score) / (Select Count(DISTINCT label) From Course )) AS AverageScore, case when(sum(student_score) / (Select Count(DISTINCT label) From Course)) >= 5.0 then 'pass'  when(sum(student_score) / (Select Count(DISTINCT label) From Course)) < 5.0 then 'fail' else NULL END AS Result from score, std, Course where score.student_id = std.Id AND Course.Id = score.course_id group by std.Id  Select count(Result) from average Where Result = 'fail' DELETE FROM dbo.average; ");
        }
    }
}
