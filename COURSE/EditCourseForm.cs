﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class EditCourseForm : Form
    {
        public EditCourseForm()
        {
            InitializeComponent();
        }

        COURSE course = new COURSE();

        private void EditCourseForm_Load(object sender, EventArgs e)
        {
            // populate the combobox with courses
            comboBoxCourse.DataSource = course.getAllCourse();
            comboBoxCourse.DisplayMember = "label";
            comboBoxCourse.ValueMember = "Id";

            // set the selected combo item to nothing
            comboBoxCourse.SelectedItem = null;
        }

        // create the function to populate the combobox
        public void fillCombo(int index)
        {
            comboBoxCourse.DataSource = course.getAllCourse();
            comboBoxCourse.DisplayMember = "label";
            comboBoxCourse.ValueMember = "Id";

            comboBoxCourse.SelectedIndex = index;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                // update the selected course
                string name = txtLabel.Text;
                int id = (int)comboBoxCourse.SelectedValue;
                string descr = txtDescription.Text;
                int hrs = (int)numericUpDownHours.Value;

                if (hrs < 10)
                {
                    MessageBox.Show("The period must be greater than 10", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (name.Trim() != "")
                {
                    // check if the course name already exists and it's not the current course using the id
                    if (!course.checkCourseName(name, id))
                    {
                        MessageBox.Show("This Course Name Already Exists", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                    else if (course.updateCourse(id, name, hrs, descr))
                    {
                        MessageBox.Show("Course Updated", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        fillCombo(comboBoxCourse.SelectedIndex);
                    }
                    else
                    {
                        MessageBox.Show("Course Not Updated", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
                else
                {
                    MessageBox.Show("Enter The Course Name", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            catch
            {
                MessageBox.Show("No Course Selected", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comboBoxCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // display the selected course data
                int id = Convert.ToInt32(comboBoxCourse.SelectedValue);
                DataTable table = new DataTable();
                table = course.getCourseById(id);
                txtLabel.Text = table.Rows[0][1].ToString();
                numericUpDownHours.Value = int.Parse(table.Rows[0][2].ToString());
                txtDescription.Text = table.Rows[0][3].ToString();
            }
            catch { }
        }
    }
}
