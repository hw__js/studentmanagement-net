﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Lab08
{
    public partial class PrintCoursesForm : Form
    {
        public PrintCoursesForm()
        {
            InitializeComponent();
        }

        private void PrintCoursesForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'myDBCourseDataSet.Course' table. You can move, or remove it, as needed.
            //this.courseTableAdapter.Fill(this.myDBCourseDataSet.Course);
            // populate datagridview with courses
            COURSE course = new COURSE();
            dataGridView1.DataSource = course.getAllCourse();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            //PrintDialog pDlg = new PrintDialog();
            //PrintDocument pDoc = new PrintDocument();
            //pDoc.DocumentName = "Print Document";
            //pDlg.Document = pDoc;
            //pDlg.AllowSelection = true;
            //pDlg.AllowSomePages = true;
            //if (pDlg.ShowDialog() == DialogResult.OK)
            //{
            //    pDoc.Print();
            //}
            //else
            //{
            //    MessageBox.Show("Đã hủy in");
            //}


            // our file path
            // the file name = courses_list.txt
            // location = in the desktop
            //D:\spkt\nam3(ki2)\WindowPrograming\week1\Lab08\textprint\courses_list.txt
            //string path = @"D:\spkt\nam3(ki2)\WindowPrograming\week1\Lab08\textprint\courses_list.docx";
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\courses_list.txt";

            using (var writer = new StreamWriter(path))
            {
                // check if the file exists
                if (!File.Exists(path))
                {
                    File.Create(path);
                }


                // rows
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    // columns
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        writer.Write("\t" + dataGridView1.Rows[i].Cells[j].Value.ToString() + "\t" + "|");
                    }
                    // Make a new line 
                    writer.WriteLine("");
                    // Make a seperation
                    //writer.WriteLine("-------------------------------------------------------------");
                }
                writer.Close();
                MessageBox.Show("Data Exported");
            }
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PDF (*.pdf)|*.pdf";
                sfd.FileName = "Output.pdf"; bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            PdfPTable pdfTable = new PdfPTable(dataGridView1.Columns.Count);
                            pdfTable.DefaultCell.Padding = 3;
                            pdfTable.WidthPercentage = 100;
                            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
                            foreach (DataGridViewColumn column in dataGridView1.Columns)
                            {
                                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                                pdfTable.AddCell(cell);
                            }
                            foreach (DataGridViewRow row in dataGridView1.Rows)
                            {
                                string id = row.Cells[0].Value.ToString();
                                pdfTable.AddCell(id);
                                string Fname = row.Cells[1].Value.ToString();
                                pdfTable.AddCell(Fname);
                                string Lname = row.Cells[2].Value.ToString();
                                pdfTable.AddCell(Lname);
                                string Bdate = row.Cells[3].Value.ToString();
                                pdfTable.AddCell(Bdate);


                            }
                            using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                            {
                                Document pdfDoc = new Document(PageSize.A4, 10f, 20f, 20f, 10f);
                                PdfWriter.GetInstance(pdfDoc, stream); pdfDoc.Open();
                                pdfDoc.Add(pdfTable);
                                pdfDoc.Close();
                                stream.Close();
                            }

                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }
    }
}
