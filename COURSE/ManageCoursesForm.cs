﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class ManageCoursesForm : Form
    {
        public ManageCoursesForm()
        {
            InitializeComponent();
        }
        COURSE course = new COURSE();
        int pos;
        private void ManageCoursesForm_Load(object sender, EventArgs e)
        {
            reloadListBoxData();
        }

        // create a function to load the listbox with courses
        public void reloadListBoxData()
        {
            listBoxCourses.DataSource = course.getAllCourse();
            listBoxCourses.ValueMember = "Id";
            listBoxCourses.DisplayMember = "label";

            // unselect the item from listbox
            listBoxCourses.SelectedItem = null;

            // display the total course
            labelTotalCourses.Text = "Total Course : " + course.totalCourses();
        }

        // create a function to display course data epending on the index
        void showData(int index)
        {
            DataRow dr = course.getAllCourse().Rows[index];

            listBoxCourses.SelectedIndex = index;
            textBoxID.Text = dr.ItemArray[0].ToString();
            txtLabel.Text = dr.ItemArray[1].ToString();
            numericUpDownHours.Value = int.Parse(dr.ItemArray[2].ToString());
            txtDescription.Text = dr.ItemArray[3].ToString();
        }
        private void listBoxCourses_Click(object sender, EventArgs e)
        {
            // display the selected course data
            pos = listBoxCourses.SelectedIndex;
            showData(pos);
        }

        // button first
        private void buttonFirst_Click(object sender, EventArgs e)
        {
            pos = 0;
            showData(0);
        }

        // button next
        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (pos < (course.getAllCourse().Rows.Count - 1))
            {
                pos = pos + 1;
                showData(pos);
            }
        }

        // button previous
        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            if (pos > 0)
            {
                pos = pos - 1;
                showData(pos);
            }
        }

        // button last
        private void buttonLast_Click(object sender, EventArgs e)
        {
            pos = course.getAllCourse().Rows.Count - 1;
            showData(pos);
        }

        // button add course
        private void buttonAddCourse_Click(object sender, EventArgs e)
        {
            if (textBoxID.Text.Trim() == "")
            {
                MessageBox.Show("Please Fill All Information", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            COURSE course = new COURSE();
            int Cid = Convert.ToInt32(textBoxID.Text);
            string name = txtLabel.Text;
            int hrs = (int)numericUpDownHours.Value;
            string descr = txtDescription.Text;

            if (name.Trim() == "") // lam viec voi string xoa het cac khoang trang truoc sau chi lay ten
            {
                MessageBox.Show("Add A Course Name", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (hrs < 10)
            {
                MessageBox.Show("The period must be greater than 10", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (course.checkCourseName(name))
            {
                if (course.insertCourse(Cid, name, hrs, descr))
                {
                    MessageBox.Show("New Course Inserted", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    reloadListBoxData();
                }
                else
                {
                    MessageBox.Show("Course Not Inserted", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("This Course Name Already Exists", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        // button edit course
        private void buttonEditCourse_Click(object sender, EventArgs e)
        {
            try
            {
                // update the selected course
                string name = txtLabel.Text;
                int id = Convert.ToInt32(textBoxID.Text);
                string descr = txtDescription.Text;
                int hrs = (int)numericUpDownHours.Value;

                if (hrs < 10)
                {
                    MessageBox.Show("The period must be greater than 10", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (name.Trim() != "")
                {
                    // check if the course name already exists and it's not the current course using the id
                    if (!course.checkCourseName(name, id))
                    {
                        MessageBox.Show("This Course Name Already Exists", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                    else if (course.updateCourse(id, name, hrs, descr))
                    {
                        MessageBox.Show("Course Updated", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        reloadListBoxData();
                    }
                    else
                    {
                        MessageBox.Show("Course Not Updated", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
                else
                {
                    MessageBox.Show("Enter The Course Name", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            catch
            {
                MessageBox.Show("No Course Selected", "Edit Course ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            pos = 0;
        }

        // button remove course
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                int courseID = Convert.ToInt32(textBoxID.Text);
                COURSE course = new COURSE();
                if (MessageBox.Show("Are You Sure You Want To Remove This Course", "Delete Course", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (course.deleteCourse(courseID))
                    {
                        MessageBox.Show("Course Deleted", " Remove Course", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        reloadListBoxData();
                        // clear fields
                        textBoxID.Text = "";
                        numericUpDownHours.Value = 10;
                        txtLabel.Text = "";
                        txtDescription.Text = "";
                    }
                    else
                    {
                        MessageBox.Show("Course not Deleted", "Remove Course", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Enter A Valid Numeric ID", "Remove Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            pos = 0;
        }


        // Can use this instead Click event
        //private void listBoxCourses_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //display the selected course data
        //        pos = listBoxCourses.SelectedIndex;
        //        showData(pos);
        //    }
        //    catch
        //    {

        //    }
        //}
    }
}
