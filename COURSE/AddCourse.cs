﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class AddCourse : Form
    {
        public AddCourse()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(txtCourseID.Text.Trim() == "" || txtPeriod.Text.Trim() == "")
            {
                MessageBox.Show("Please Fill All Information", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            COURSE course = new COURSE();
            int Cid = Convert.ToInt32(txtCourseID.Text);
            string name = txtLabel.Text;
            int hrs = Convert.ToInt32(txtPeriod.Text);
            string descr = txtDescription.Text;

            if (name.Trim() == "") // lam viec voi string xoa het cac khoang trang truoc sau chi lay ten
            {
                MessageBox.Show("Add A Course Name", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (hrs < 10)
            {
                MessageBox.Show("The period must be greater than 10", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (course.checkCourseName(name))
            {
                if (course.insertCourse(Cid, name, hrs, descr))
                {
                    MessageBox.Show("New Course Inserted", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Course Not Inserted", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("This Course Name Already Exists", "Add Course", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
