﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bt_Login_Click(object sender, EventArgs e)
        {
            //if (radioButtonStudent.Checked == true)
            //{
            //    MY_DB db = new MY_DB();
            //    SqlDataAdapter adapter = new SqlDataAdapter();
            //    DataTable table = new DataTable();
            //    SqlCommand command = new SqlCommand("SELECT * FROM login WHERE username = @User AND password = @Pass", db.getConnection);

            //    command.Parameters.Add("@User", SqlDbType.VarChar).Value = TextBoxUsername.Text;
            //    command.Parameters.Add("@Pass", SqlDbType.VarChar).Value = TextBoxPassword.Text;

            //    adapter.SelectCommand = command;
            //    adapter.Fill(table);

            //    if ((table.Rows.Count > 0))
            //    {
            //         MessageBox.Show("Ok, next time will be go to Main Menu of App");
            //         của thầy(chỉ dùng dòng này là vào thẳng form main) nếu không dùng timer để làm progressbar: this.DialogResult = DialogResult.OK;

            //        int userid = Convert.ToInt16(table.Rows[0][0].ToString());
            //         dùng 1 lớp static là Global class, lớp này dùng để lấy và gọi Id trong trong 
            //        Globals.SetGlobalUserId(userid);

            //        progressBar1.Visible = true;
            //        timer1.Start();
            //    }
            //    else
            //    {
            //        MessageBox.Show("Invalid Username or Password", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
            MY_DB db = new MY_DB();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("SELECT * FROM hr WHERE uname = @User AND pwd = @Pass", db.getConnection);

            command.Parameters.Add("@User", SqlDbType.VarChar).Value = TextBoxUsername.Text;
            command.Parameters.Add("@Pass", SqlDbType.VarChar).Value = TextBoxPassword.Text;

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if ((table.Rows.Count > 0))
            {
                // MessageBox.Show("Ok, next time will be go to Main Menu of App");
                // của thầy(chỉ dùng dòng này là vào thẳng form main) nếu không dùng timer để làm progressbar: this.DialogResult = DialogResult.OK;

                int userid = Convert.ToInt16(table.Rows[0][0].ToString());
                // dùng 1 lớp static là Global class, lớp này dùng để lấy và gọi Id trong trong 
                Globals.SetGlobalUserId(userid);
                progressBar1.Visible = true;
                timer1.Start();
            }
            else
            {
                MessageBox.Show("Invalid Username or Password", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // on form load 
        private void Form1_Load(object sender, EventArgs e)
        {
            // dua hinh tu folder
            //pictureBox1.Image = Image.FromFile("D:/spkt/nam3(ki2)/WindowPrograming/week1/Lab08/images/oh1.png");
            radioButtonStudent.Checked = true;
        }

        // Cài đặt phím Enter cho textbox password
        private void TextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
            {
                bt_Login.PerformClick();
            }    
        }

        // Cài đặt time cho progress Bar
        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = Convert.ToInt32(progressBar1.Value + 10);
            if (Convert.ToInt32(progressBar1.Value) == 100)
            {
                timer1.Stop();
                this.DialogResult = DialogResult.OK;
            }
        }

        private void TextBoxUsername_Validating(object sender, CancelEventArgs e)
        {
            if(string.IsNullOrEmpty(TextBoxUsername.Text))
            {
                errorProvider1.SetError(TextBoxUsername, "Please enter your username!");
            }    
            else
            {
                errorProvider1.Clear();
                //errorProvider1.SetError(TextBoxUsername, null);
            }    
        }

        private void TextBoxPassword_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxPassword.Text))
            {
                errorProvider1.SetError(TextBoxPassword, "Please enter your password!");
            }
            else
            {
                errorProvider1.Clear();
                //errorProvider1.SetError(TextBoxPassword, null);
            }
        }


        private void TextBoxUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bt_Login.PerformClick();
            }
        }

        // label click to register form
        private void bt_Register_Click(object sender, EventArgs e)
        {
            Register1 reg = new Register1();
            reg.Show(this);
        }

        private void pct_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
