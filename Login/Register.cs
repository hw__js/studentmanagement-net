﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab08
{
    public partial class Register1 : Form
    {
        public Register1()
        {
            InitializeComponent();
        }

        // chuc nang kiem tra du lieu input
        bool verify()
        {
            if ((textBoxFname.Text.Trim() == "") || (textBoxLname.Text.Trim() == "") || (textBoxUname.Text.Trim() == "") || (textBoxPassword.Text.Trim() == "") || (pictureBoxUserImage.Image == null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void pct_Box_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void labelLogin_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Close();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            User user = new User();
            //int id = Convert.ToInt32(txtStudentID.Text);
            string fname = textBoxFname.Text;
            string lname = textBoxLname.Text;
            string username = textBoxUname.Text;
            string password = textBoxPassword.Text;
            MemoryStream pic = new MemoryStream();

            if (verify() == true)
            {
                if (!user.usernameExist(textBoxUname.Text, "register"))
                {
                    pictureBoxUserImage.Image.Save(pic, pictureBoxUserImage.Image.RawFormat);
                    if (user.insertUser(fname, lname, username, password, pic) == true)
                    {
                        MessageBox.Show("Registration Completed Success!", "Register", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Something Wrong", "Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("This Username Already Exists", "Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Empty Fields", "Add User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Select Image(*.jpg;*.png;*.gif)|*.jpg;*.png;*.gif";
            if ((opf.ShowDialog() == DialogResult.OK))
            {
                pictureBoxUserImage.Image = Image.FromFile(opf.FileName);
            }
        }
    }
}
