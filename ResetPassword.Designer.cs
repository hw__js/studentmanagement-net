﻿namespace Lab08
{
    partial class ResetPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResetPassword));
            this.label_oldPass = new System.Windows.Forms.Label();
            this.label_newPass = new System.Windows.Forms.Label();
            this.label_confirmPass = new System.Windows.Forms.Label();
            this.txtBox_oldPass = new System.Windows.Forms.TextBox();
            this.txtBox_newPass = new System.Windows.Forms.TextBox();
            this.txtBox_confirmPass = new System.Windows.Forms.TextBox();
            this.btn_savePass = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_oldPass
            // 
            this.label_oldPass.AutoSize = true;
            this.label_oldPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_oldPass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_oldPass.Location = new System.Drawing.Point(28, 44);
            this.label_oldPass.Name = "label_oldPass";
            this.label_oldPass.Size = new System.Drawing.Size(140, 25);
            this.label_oldPass.TabIndex = 0;
            this.label_oldPass.Text = "Old Password:";
            // 
            // label_newPass
            // 
            this.label_newPass.AutoSize = true;
            this.label_newPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_newPass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_newPass.Location = new System.Drawing.Point(20, 121);
            this.label_newPass.Name = "label_newPass";
            this.label_newPass.Size = new System.Drawing.Size(148, 25);
            this.label_newPass.TabIndex = 1;
            this.label_newPass.Text = "New Password:";
            // 
            // label_confirmPass
            // 
            this.label_confirmPass.AutoSize = true;
            this.label_confirmPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_confirmPass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_confirmPass.Location = new System.Drawing.Point(20, 193);
            this.label_confirmPass.Name = "label_confirmPass";
            this.label_confirmPass.Size = new System.Drawing.Size(177, 25);
            this.label_confirmPass.TabIndex = 2;
            this.label_confirmPass.Text = "Confirm Password:";
            // 
            // txtBox_oldPass
            // 
            this.txtBox_oldPass.Location = new System.Drawing.Point(256, 35);
            this.txtBox_oldPass.Multiline = true;
            this.txtBox_oldPass.Name = "txtBox_oldPass";
            this.txtBox_oldPass.Size = new System.Drawing.Size(277, 34);
            this.txtBox_oldPass.TabIndex = 3;
            // 
            // txtBox_newPass
            // 
            this.txtBox_newPass.Location = new System.Drawing.Point(256, 112);
            this.txtBox_newPass.Multiline = true;
            this.txtBox_newPass.Name = "txtBox_newPass";
            this.txtBox_newPass.Size = new System.Drawing.Size(277, 34);
            this.txtBox_newPass.TabIndex = 4;
            // 
            // txtBox_confirmPass
            // 
            this.txtBox_confirmPass.Location = new System.Drawing.Point(256, 184);
            this.txtBox_confirmPass.Multiline = true;
            this.txtBox_confirmPass.Name = "txtBox_confirmPass";
            this.txtBox_confirmPass.Size = new System.Drawing.Size(277, 34);
            this.txtBox_confirmPass.TabIndex = 5;
            // 
            // btn_savePass
            // 
            this.btn_savePass.BackColor = System.Drawing.Color.LightSalmon;
            this.btn_savePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_savePass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_savePass.Location = new System.Drawing.Point(144, 251);
            this.btn_savePass.Name = "btn_savePass";
            this.btn_savePass.Size = new System.Drawing.Size(131, 54);
            this.btn_savePass.TabIndex = 6;
            this.btn_savePass.Text = "OK";
            this.btn_savePass.UseVisualStyleBackColor = false;
            this.btn_savePass.Click += new System.EventHandler(this.btn_savePass_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btn_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Cancel.Location = new System.Drawing.Point(356, 251);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(126, 54);
            this.btn_Cancel.TabIndex = 7;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 8;
            // 
            // ResetPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(618, 327);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_savePass);
            this.Controls.Add(this.txtBox_confirmPass);
            this.Controls.Add(this.txtBox_newPass);
            this.Controls.Add(this.txtBox_oldPass);
            this.Controls.Add(this.label_confirmPass);
            this.Controls.Add(this.label_newPass);
            this.Controls.Add(this.label_oldPass);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResetPassword";
            this.Text = "ResetPassword";
            this.Load += new System.EventHandler(this.ResetPassword_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_oldPass;
        private System.Windows.Forms.Label label_newPass;
        private System.Windows.Forms.Label label_confirmPass;
        private System.Windows.Forms.TextBox txtBox_oldPass;
        private System.Windows.Forms.TextBox txtBox_newPass;
        private System.Windows.Forms.TextBox txtBox_confirmPass;
        private System.Windows.Forms.Button btn_savePass;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label label1;
    }
}